# encoding=gbk
from __future__ import print_function, absolute_import
from gm.api import *
from datetime import datetime,timedelta
import time
import pandas as pd

# 定义变量
A = 2
B = 21
C = 3 
D = 15
E = 260
I = 60
F = 21
J = 1 # 默认为3暂时先设03
G = 0.03
H = 0.05
K = 600 # 市值，单位亿元

BACKTEST = False

def algo(context):
    print(context.now)
    # 获取所有标的 似乎还要去掉200和900的B股
    all_symbols = get_instruments(exchanges='SZSE,SHSE', sec_types=1, fields='symbol',df=1)['symbol'].tolist()

    # 3PETTM 过滤部分不满足pe的标的
    PE = get_fundamentals_n(table='trading_derivative_indicator', symbols=all_symbols, end_date=context.now.strftime('%Y-%m-%d'), count=1, fields='PETTM',df=True)

    # 市值
    # CAP = get_fundamentals_n(table='trading_derivative_indicator', symbols=all_symbols, end_date=context.now.strftime('%Y-%m-%d'), count=1, fields='TOTMKTCAP',df=True)
    all_symbols = PE.loc[PE['PETTM'] < I, 'symbol'].to_list()
    # temp = CAP.loc[CAP['TOTMKTCAP'] < K, 'symbol'].to_list()

    # 两个都符合才继续判断
    # all_symbols = list(set(all_symbols) & set(all_symbols))


    symbol_str = ''
    for symbol in all_symbols:
        symbol_str += symbol
        symbol_str += ','
    symbol_str.strip(',')

    count = max(B, E, F, 90)  # 最少获取90天，后面需要用到两个月的数据
    today = context.now

    all_upper = {}
    num = int(30000 / F)
    temp_symbols = all_symbols[:]

    # 分片获取所有标的的历史instrument
    while temp_symbols:
        temp_list = []
        for i in range(num):
            if temp_symbols:
                temp_list.append(temp_symbols.pop())
            else:
                break
        
        temp_instrument = get_history_instruments(symbols=temp_list, fields='symbol,trade_date,upper_limit',
         start_date=(context.now - timedelta(days=F)).strftime('%Y-%m-%d'), df=True)
        
        for symbol in temp_list:
            all_upper[symbol] = temp_instrument.loc[temp_instrument['symbol'] == symbol, :].reset_index(drop=True)
    
    
    all_his = {}
    num = int(30000 / count)
    temp_symbols = all_symbols[:]
    start_date = today - timedelta(days= 2 * count)

    trade_date = get_trading_dates('SZSE', start_date=start_date.strftime('%Y-%m-%d'), end_date=today.strftime('%Y-%m-%d'))
    start_date = trade_date[-count]


    # 分片获取所有标的的历史instrument
    while temp_symbols:
        temp_str = ''
        for i in range(num):
            if temp_symbols:
                temp_str += temp_symbols.pop()
                temp_str += ','
            else:
                break
        temp_str = temp_str.strip(',')
        temp_his = history(symbol=temp_str, frequency='1d', 
        start_time=start_date, end_time=today, fields='symbol, close, high, volume, eob', adjust=ADJUST_PREV, df= True)
        
        for symbol in temp_str.split(','):
            all_his[symbol] = temp_his.loc[temp_his['symbol'] == symbol, :].reset_index(drop=True)
        # print(temp_str)
    
    res = []
    for k, symbol in enumerate(all_symbols):
        # print(k, symbol)
        symbol_upper = all_upper.get(symbol, pd.DataFrame())
        history_n_data = all_his.get(symbol, pd.DataFrame())
        if symbol_upper.empty or history_n_data.empty:
            continue
        # 近段时间的涨停次数
        if history_n_data.shape[0] != count:
            # print(f'{symbol} 该股新上不足一年，不考虑~')
            continue  # 新股上市，数据不够长。
        avg_volume = history_n_data.loc[history_n_data.shape[0] - B, 'volume'].mean()
        
        volume_mark = True
        # 1检察最近的成交量是否大于长期成交量
        for i in range(A):
            if history_n_data.loc[history_n_data.shape[0] - i -1, 'volume'] <= C * avg_volume:
                volume_mark = False
                break
        if not volume_mark:
            continue
        
        # print('0')
        # 2短周期新高是否是长周期新高
        if history_n_data.loc[history_n_data.shape[0] - D:, 'high'].max() \
        != history_n_data.loc[history_n_data.shape[0] - E:, 'high'].max():
            continue
        
        # print('1')
        # 涨停次数
        upper_count = 0
        # symbol_upper = all_upper.loc[all_upper['symbol'] == symbol, ['upper_limit', 'trade_date']]
        for index, row in history_n_data.iterrows():
            for index_upper, row_upper in symbol_upper.iterrows():
                if row['eob'] == row_upper['trade_date']:
                    if row_upper['upper_limit'] == row['close']:  # 收盘价等于涨停
                        upper_count += 1
        if upper_count < J:
            continue

        # print('2')
        # 逆向循环计算月收益
        income1 = 0
        income2 = 0
        place = []
        for i in range(history_n_data.shape[0] - 1, 0, -1):
            # 计算月线收益率
            if history_n_data.loc[i, 'eob'].month != history_n_data.loc[i-1, 'eob'].month:
            # 计算周线收益率
            # if history_n_data.loc[i, 'eob'].week != history_n_data.loc[i-1, 'eob'].week:
                place.append(i-1)
                if len(place) == 3:
                    break
        income1 = history_n_data.loc[place[0], 'close'] / history_n_data.loc[place[1], 'close']
        income2 = history_n_data.loc[place[1], 'close'] / history_n_data.loc[place[2], 'close']

        if not (income1 > G and income2 > H):
            continue

        # 满足前面所有条件，则记录
        res.append(symbol)
    print(f'时间是：{time.ctime()}，选中标的是：{res}, 检测了{k}个')


    if BACKTEST:
        send_order(context, res)
    else:
        with open('股票池.txt', 'w') as f:
            for item in res:
                f.write(item)
                f.write('\n')


def send_order(context, res):

    for symbol in res[:10]:
        position = context.account().position(symbol=symbol, side=PositionSide_Long)

        if not position:
            buy_percent = context.ratio
            order_target_percent(symbol=symbol, percent=buy_percent, order_type=OrderType_Market,
                                 position_side=PositionSide_Long)
            print(symbol, '以市价单开多仓至仓位:', buy_percent)

    Account_positions = context.account().positions()
    for pos in Account_positions:
        if pos.symbol not in res:
            order_target_percent(symbol=pos.symbol, percent=0, order_type=OrderType_Market,
                                position_side=PositionSide_Long)

def init(context):
    # 每天14:50 定时执行algo任务,
    # algo执行定时任务函数，只能传context参数
    # date_rule执行频率，目前暂时支持1d、1w、1m，其中1w、1m仅用于回测，实时模式1d以上的频率，需要在algo判断日期
    # time_rule执行时间， 注意多个定时任务设置同一个时间点，前面的定时任务会被后面的覆盖
    schedule(schedule_func=algo, date_rule='1w', time_rule='14:50:00')
    context.ratio = 0.1

# 查看最终的回测结果
def on_backtest_finished(context, indicator):
    print(indicator)

if __name__ == '__main__':
    '''
        strategy_id策略ID, 由系统生成
        filename文件名, 请与本文件名保持一致
        mode运行模式, 实时模式:MODE_LIVE回测模式:MODE_BACKTEST
        token绑定计算机的ID, 可在系统设置-密钥管理中生成
        backtest_start_time回测开始时间
        backtest_end_time回测结束时间
        backtest_adjust股票复权方式, 不复权:ADJUST_NONE前复权:ADJUST_PREV后复权:ADJUST_POST
        backtest_initial_cash回测初始资金
        backtest_commission_ratio回测佣金比例
        backtest_slippage_ratio回测滑点比例
    '''
    if BACKTEST:
        run(strategy_id='a822bf5b-1aac-11ec-b603-b06ebf8309fa',
        filename='main.py',
        mode=MODE_BACKTEST,
        token='59573ca54455aea997843ea701144020acbf2870',
        backtest_start_time='2019-9-01 08:00:00',
        backtest_end_time='2021-9-21 10:00:00',
        backtest_adjust=ADJUST_PREV,
        backtest_initial_cash=10000000,
        backtest_commission_ratio=0.0000,
        backtest_slippage_ratio=0.0000)
    else:
        set_token('59573ca54455aea997843ea701144020acbf2870')
        class Context:
            pass
        context = Context()
        context.now = datetime.now()
        algo(context)